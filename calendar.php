<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            #days{position:absolute;width:340px;height:280px;background:-moz-linear-gradient(bottom,#23b3f8,#33d3f5);background:-webkit-linear-gradient(bottom,#23b3f8,#33d3f5);}
            #days .calheader{width:316px;height:76px;background:url("image/calendar_header.png") no-repeat center center;background-size:cover;margin:auto}
            #days .calbody{width:316px;height:204px;margin:auto;background-color:#FFFFFF;}
            .calbody .week{float:left;width:316px;list-style:none;padding:0;margin:auto;}
            .calbody .week .day{float:left;text-align:center;font-size:13px;width:45px;height:40px;line-height:40px;}
        </style>
    </head>
    <body>
        <?php
        // put your code here
        include 'vendor/autoload.php';//composer 的默认安装路径
        
        $_year = date("Y");
        $_month = date("m");
        $factory = new CalendR\Calendar;
        $factory->setFirstWeekday(CalendR\Period\Day::SUNDAY);
        $month = $factory->getMonth($_year, $_month);
        $days = showDays($month);
        echo $days;

        function showDays($month, $signined = array())
        {
            $days = '<div id="days" class="days">';
            $days.='<div class="calheader"></div>';
            $days.='<div class="calbody">';
            foreach ($month as $week) {
                $days.='<ul class="week">';
                foreach ($week as $key => $day) {
                    if ($month->includes($day)) {
                        if (in_array($key, $signined)) {
                            $days.='<li class="day signined">' . date("j", strtotime($key)) . '</li>';
                        } else {
                            $days.='<li class="day">' . date("j", strtotime($key)) . '</li>';
                        }
                    } else {
                        $days.='<li class="day"></li>';
                    }
                }
                $days.='</ul>';
            }
            $days.='</div>';
            $days.='</div>';
            return $days;
        }
        ?>
    </body>

</html>
